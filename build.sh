#!/usr/bin/zsh

c_file=src/main.c
executable="db"
gcc -o $executable $c_file

if [ $? -eq 0 ]; then
    echo "Compilation successful"
else 
    echo "Compilation failed"
fi