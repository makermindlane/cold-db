# Cold-db
A simple database implementation to understand how database works. Here is the [Blogpost](https://cstack.github.io/db_tutorial) to follow.

# Commands
## Build
- ./build.sh

## Run
- ./cold-db

Note: `build.sh` needs to be executable. Use `chmod +x build.sh` to do it.
