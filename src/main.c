#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>

typedef enum { MetaCommandResultSuccess, MetaCommandResultUnrecognizedCommand } MetaCommandResult;

typedef enum {
    PrepareResultSuccess,
    PrepareResultNegativeId,
    PrepareResultUnrecognizedStatement,
    PrepareResultSyntaxError,
    PrepareResultStringTooLong,
} PrepareResult;

typedef enum { StatementTypeInsert, StatementTypeSelect } StatementType;

#define COLUMN_USERNAME_SIZE 32
#define COLUMN_EMAIL_SIZE    255

typedef struct {
    uint32_t id;
    char username[COLUMN_USERNAME_SIZE + 1];
    char email[COLUMN_EMAIL_SIZE + 1];
} Row;

#define size_of_attribute(struct, attribute) sizeof(((struct*)0)->attribute)

const uint32_t ID_SIZE = size_of_attribute(Row, id);
const uint32_t USERNAME_SIZE = size_of_attribute(Row, username);
const uint32_t EMAIL_SIZE = size_of_attribute(Row, email);
const uint32_t ID_OFFSET = 0;
const uint32_t USERNAME_OFFSET = ID_OFFSET + ID_SIZE;
const uint32_t EMAIL_OFFSET = USERNAME_OFFSET + USERNAME_SIZE;
const uint32_t ROW_SIZE = ID_SIZE + USERNAME_SIZE + EMAIL_SIZE;

typedef struct {
    StatementType type;
    Row row_to_insert;
} Statement;

typedef struct InputBuffer {
    char* buffer;
    size_t buffer_length;
    ssize_t input_length;
} InputBuffer;

InputBuffer* new_input_buffer() {
    InputBuffer* input_buffer = (InputBuffer*)malloc(sizeof(InputBuffer));
    input_buffer->buffer = NULL;
    input_buffer->buffer_length = 0;
    input_buffer->input_length = 0;

    return input_buffer;
}

void serialize_row(Row* source, void* destination) {
    memcpy(destination + ID_OFFSET, &source->id, ID_SIZE);
    strncpy(destination + USERNAME_OFFSET, &source->username, USERNAME_SIZE);
    strncpy(destination + EMAIL_OFFSET, &source->email, EMAIL_SIZE);
}

void deserialize_row(void* source, Row* destination) {
    memcpy(&destination->id, source + ID_OFFSET, ID_SIZE);
    memcpy(&destination->username, source + USERNAME_OFFSET, USERNAME_SIZE);
    memcpy(&destination->email, source + EMAIL_OFFSET, EMAIL_SIZE);
}

#define PAGE_PER_TABLE 100
const uint32_t PAGE_SIZE = 4096;
const uint32_t ROW_PER_PAGE = PAGE_SIZE / ROW_SIZE;
const uint32_t ROW_PER_TABLE = ROW_PER_PAGE * PAGE_PER_TABLE;

typedef struct {
    int file_descriptor;
    uint32_t file_length;
    void* pages[PAGE_PER_TABLE];
} Pager;

#include <sys/file.h>
#include <sys/stat.h>
#include <errno.h>
#include <unistd.h>

Pager* pager_open(const char* filename) {
    int fd = open(filename,
                  O_RDWR |      // Read/Write mode
                      O_CREAT,  // Create file if it does not exist
                  S_IWUSR |     // User write permission
                      S_IRUSR   // User read permission
    );
    if (fd == -1) {
        printf("Unable to open file\n");
        exit(EXIT_FAILURE);
    }
    
    off_t file_length = lseek(fd, 0, SEEK_END);

    Pager* pager = malloc(sizeof(Pager));
    pager->file_descriptor = fd;
    pager->file_length = file_length;

    for (uint32_t i = 0; i < PAGE_PER_TABLE; ++i) {
        pager->pages[i] = NULL;
    }

    return pager;
}

void pager_flush(Pager* pager, uint32_t page_num, uint32_t size) {
    if (pager->pages[page_num] == NULL) {
        printf("Tried to flush null page\n");
        exit(EXIT_FAILURE);
    }

    off_t offset = lseek(pager->file_descriptor, page_num * PAGE_SIZE, SEEK_SET);
    if (offset == -1) {
        printf("Error seeking: %d\n", errno);
        exit(EXIT_FAILURE);
    }

    ssize_t bytes_written = write(pager->file_descriptor, pager->pages[page_num], size);
    if (bytes_written == -1) {
        printf("Error writing: %d\n", errno);
        exit(EXIT_FAILURE);
    }
}

void* get_page(Pager* pager, uint32_t page_num) {
    if (page_num > PAGE_PER_TABLE) {
        printf("Tried to fetch page number out of bounds. %d > %d\n", page_num, PAGE_PER_TABLE);
        exit(EXIT_FAILURE);
    }

    if (pager->pages[page_num] == NULL) {
        // Cache miss. ALlocate memory and load from file.
        void* page = malloc(PAGE_SIZE);
        uint32_t num_pages = pager->file_length / PAGE_SIZE;

        // We might save a partial page at the end of the file.
        if (pager->file_length % PAGE_SIZE) {
            num_pages += 1;
        }

        if (page_num <= num_pages) {
            lseek(pager->file_descriptor, page_num * PAGE_SIZE, SEEK_SET);
            ssize_t bytes_read = read(pager->file_descriptor, page, PAGE_SIZE);
            if (bytes_read == -1) {
                printf("Error reading file: %d\n", errno);
                exit(EXIT_FAILURE);
            }
        }

        pager->pages[page_num] = page;
    }

    return pager->pages[page_num];
}

typedef struct {
    Pager* pager;
    uint32_t num_rows;
} Table;

Table* db_open(const char* filename) {
    Pager* pager = pager_open(filename);
    uint32_t num_rows = pager->file_length / ROW_SIZE;

    Table* table = (Table*)malloc(sizeof(Table));
    table->pager = pager;
    table->num_rows = num_rows;
    // for (uint32_t i = 0; i < PAGE_PER_TABLE; ++i) {
    //     table->pages[i] = NULL;
    // }
    return table;
}

void db_close(Table* table) {
    Pager* pager = table->pager;
    uint32_t num_full_pages = table->num_rows / ROW_PER_PAGE;

    for (uint32_t i = 0; i < num_full_pages; ++i) {
        if (pager->pages[i] == NULL) {
            continue;
        }
        pager_flush(pager, i, PAGE_SIZE);
        free(pager->pages[i]);
        pager->pages[i] = NULL;
    }

    // There may be a partial page to write to the end of the file
    // This should not be needed after we switch to a B-tree
    uint32_t num_additional_rows = table->num_rows % ROW_PER_PAGE;
    if (num_additional_rows > 0) {
        uint32_t page_num = num_full_pages;
        if (pager->pages[page_num] != NULL) {
            pager_flush(pager, page_num, num_additional_rows * ROW_SIZE);
            free(pager->pages[page_num]);
            pager->pages[page_num] = NULL;
        }
    }

    int result = close(pager->file_descriptor);
    if (result == -1) {
        printf("Error closng db file.\n");
        exit(EXIT_FAILURE);
    }
    for (uint32_t i = 0; i < PAGE_PER_TABLE; ++i) {
        void* page = pager->pages[i];
        if (page) {
            free(page);
            pager->pages[i] = NULL;
        }
    }
    free(pager);
    free(table);
}

// void free_table(Table* table) {
//     for (int i = 0; table->pages[i]; ++i) {
//         free(table->pages[i]);
//     }
//     free(table);
// }

void close_input_buffer(InputBuffer* const self) {
    free(self->buffer);
    free(self);
}

void read_input(InputBuffer* const self) {
    ssize_t bytes_read = getline(&self->buffer, &self->buffer_length, stdin);
    if (bytes_read <= 0) {
        printf("Error reading input\n");
        exit(EXIT_FAILURE);
    }
    self->input_length = bytes_read - 1;
    self->buffer[bytes_read - 1] = 0;
}

MetaCommandResult do_meta_command(InputBuffer* const input_buf, Table* table) {
    if (strcmp(input_buf->buffer, ".exit") == 0) {
        close_input_buffer(input_buf);
        db_close(table);
        exit(EXIT_SUCCESS);
    } else {
        return MetaCommandResultUnrecognizedCommand;
    }
}

PrepareResult prepare_insert(InputBuffer* const input_buf, Statement* const statement) {
    statement->type = StatementTypeInsert;

    char* keyword = strtok(input_buf->buffer, " ");
    char* id_str = strtok(NULL, " ");
    char* username = strtok(NULL, " ");
    char* email = strtok(NULL, " ");

    if (id_str == NULL || username == NULL || email == NULL) {
        return PrepareResultSyntaxError;
    }

    int id = atoi(id_str);
    if (id < 0) {
        return PrepareResultNegativeId;
    }
    if (strlen(username) > COLUMN_USERNAME_SIZE) {
        return PrepareResultStringTooLong;
    }

    statement->row_to_insert.id = id;
    strcpy(statement->row_to_insert.username, username);
    strcpy(statement->row_to_insert.email, email);

    return PrepareResultSuccess;
}

PrepareResult prepare_statement(InputBuffer* const input_buf, Statement* const statement) {
    if (strncmp(input_buf->buffer, "insert", 6) == 0) {
        return prepare_insert(input_buf, statement);
    }

    if (strcmp(input_buf->buffer, "select") == 0) {
        statement->type = StatementTypeSelect;
        return PrepareResultSuccess;
    }

    return PrepareResultUnrecognizedStatement;
}

void* row_slot(Table* table, uint32_t row_num) {
    uint32_t page_num = row_num / ROW_PER_PAGE;
    void* page = get_page(table->pager, page_num);
    uint32_t row_offset = row_num % ROW_SIZE;
    uint32_t byte_offset = row_offset * ROW_SIZE;
    return page + byte_offset;
}

typedef enum { ExecuteResultSuccess, ExecuteResultTableFull } ExecuteResult;

ExecuteResult execute_insert(Statement* statement, Table* table) {
    if (table->num_rows >= ROW_PER_TABLE) {
        return ExecuteResultTableFull;
    }

    Row* const row_to_insert = &statement->row_to_insert;
    serialize_row(row_to_insert, row_slot(table, table->num_rows));
    table->num_rows += 1;

    return ExecuteResultSuccess;
}

void print_row(const Row* const row) {
    printf("(%u, %s, %s)\n", row->id, row->username, row->email);
}

ExecuteResult execute_select(Statement* statement, Table* table) {
    Row row;
    for (uint32_t i = 0; i < table->num_rows; ++i) {
        deserialize_row(row_slot(table, i), &row);
        print_row(&row);
    }
    return ExecuteResultSuccess;
}

ExecuteResult execute_statement(Statement* statement, Table* table) {
    switch (statement->type) {
        case StatementTypeInsert:
            return execute_insert(statement, table);

        case StatementTypeSelect:
            return execute_select(statement, table);
    }
}

void print_prompt() { printf("db > "); }

int main(int argc, char* argv[]) {
    if (argc < 2) {
        printf("Must supply a database filename.\n");
        exit(EXIT_FAILURE);
    }

    char* filename = argv[1];
    Table* table = db_open(filename);
    InputBuffer* input_buf = new_input_buffer();
    while (1) {
        print_prompt();
        read_input(input_buf);

        if (input_buf->buffer[0] == '.') {
            switch (do_meta_command(input_buf, table)) {
                case MetaCommandResultSuccess:
                    continue;

                case MetaCommandResultUnrecognizedCommand:
                    printf("Unrecognized command '%s'\n", input_buf->buffer);
                    continue;
            }
        }

        Statement statement;
        switch (prepare_statement(input_buf, &statement)) {
            case PrepareResultSuccess:
                break;

            case PrepareResultNegativeId:
                printf("ID must be positive.\n");
                continue;

            case PrepareResultStringTooLong:
                printf("String is too long.\n");
                continue;

            case PrepareResultSyntaxError:
                printf("Syntax error. Could not parse statement.\n");
                continue;

            case PrepareResultUnrecognizedStatement:
                printf("Unrecognized keyword at start of '%s'.\n", input_buf->buffer);
                continue;
        }

        switch (execute_statement(&statement, table)) {
            case ExecuteResultSuccess:
                printf("Executed.\n");
                break;

            case ExecuteResultTableFull:
                printf("Error: Table full.\n");
                break;
        }
    }
}
